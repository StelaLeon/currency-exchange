How to run it? 

Please start the docker container with one-frame container: 

        docker run -p 8080:8080 paidyinc/one-frame 

Test it's all up and running by running:

    `curl --location --request GET 'http://localhost:8080/rates?pair=USDJPY' \
    --header 'token: 10dc303535874aeccc86a8251e6992f5'`

This will return an example like:

        `[{
                "from": "USD",
                "to": "JPY",
                "bid": 0.029203487085703528,
                "ask": 0.16098382286965063,
                "price": 0.095093654977677079,
                "time_stamp": "2023-03-28T11:32:06.068Z"
        }]`

This will get parsed and served to the client via: 

        `curl   --location --request GET 'http://localhost:8081/rates?from=USD&to=EUR' \
                --header 'token: 10dc303535874aeccc86a8251e6992f5'`

At this point the service has a limitation: we only read one pair-currency at the time, this can be extended to a list of currencies. 
I use Concurrent.memoize, this would reduce the number of requests made to the external service when the service is called with a list of currency exchanges.
Due to time limitations I did not extend the functionality to a list of currencies, because if the someone calls the proxy with one currency at the time, this still does not ensure that we reduce the number of requests by a factor of 10. 
Concurrent.memoize will memorize identical consecutive currency pair.

Permanent solution for the difference in speed between proxy and one frame service involves a caching mechanism using :

        `type Cache = Map[Pair, Rate]
        getResultFromExternalService(implicit F: Stateful[F, Cache]) `
e.g.

        `override def get(pair: Rate.Pair)(implicit F: Stateful[F, Cache]): F[Either[Error, Rate]] = {
        for {
                cache <- F.get
                result <- cache.get(pair) match {
                        case Some(result) => result.pure[F]
                        case None => fetchRateCurrencyPair(pair)
                }
                _ <- F.set(cache.updated(pair, result))
        } yield result
        }`

I would suggest to use this cache and attach to it an fs2 stream that cleans it up every 5 mins in order to respect the requirements to have consistent data no older than 5 minutes. 
Doing so we can reduce the number of requests

## Tests
sbt test
There are tests only for RatesHttpRoutesSpec, we could use TestContainers to be able to build intergration tests for this "interface" between proxy and one frame

