package forex.domain

import io.circe.{Decoder, HCursor}
import io.circe.generic.semiauto.deriveDecoder

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

case class Timestamp(value: OffsetDateTime) extends AnyVal

object Timestamp {
  implicit val timestampDecoder: Decoder[Timestamp] = deriveDecoder[Timestamp]
  val timestampDecoderOneFrame: Decoder[Timestamp] = (c: HCursor) =>
    Decoder.decodeString
      .map(s => {
        val date =
          OffsetDateTime.parse(s, DateTimeFormatter.ISO_OFFSET_DATE_TIME)
        Timestamp(date)
      })
      .apply(c)

  def now: Timestamp =
    Timestamp(OffsetDateTime.now)
}
