package forex.http.rates

import forex.domain.Rate.Pair
import forex.domain.{Currency, Price, Rate, Timestamp}
import io.circe.{Decoder, HCursor}

case class OneFrameResponse(from: Currency,
                            to: Currency,
                            bid: Float,
                            ask: BigDecimal,
                            price: BigDecimal,
                            timestamp: forex.domain.Timestamp)
object OneFrameResponse {
  implicit val oneFrameResponseDecoder: Decoder[OneFrameResponse] =
    (c: HCursor) =>
      for {
        from <- c.downField("from").as[String].map(Currency.fromString)
        to <- c.downField("to").as[String].map(Currency.fromString)
        bid <- c.downField("bid").as[Float]
        ask <- c.downField("ask").as[BigDecimal]
        price <- c.downField("price").as[BigDecimal]
        timestamp <- c
          .downField("time_stamp")
          .as[forex.domain.Timestamp](Timestamp.timestampDecoderOneFrame)
      } yield {
        new OneFrameResponse(from,
                             to,
                             bid = bid,
                             ask = ask,
                             price = price,
                             timestamp = timestamp)
    }

  def toRate(oneFrameResponse: OneFrameResponse) = {
    Rate(Pair(oneFrameResponse.from, oneFrameResponse.to),
         Price(oneFrameResponse.price),
         oneFrameResponse.timestamp)
  }
}
