package forex.http.rates

import forex.domain.Currency
import org.http4s.dsl.impl.ValidatingQueryParamDecoderMatcher
import org.http4s.{ParseFailure, QueryParamDecoder}

import scala.util.Try

object QueryParams {

  private[http] implicit val currencyQueryParam: QueryParamDecoder[Currency] =
    QueryParamDecoder[String].emap[Currency](s =>
      Try(Currency.fromString(s)).toEither match {
        case Right(r)  => Right(r)
        case Left(err) => Left(ParseFailure(err.getMessage, err.getMessage))
    })

  object FromQueryParam
      extends ValidatingQueryParamDecoderMatcher[Currency]("from")
  object ToQueryParam extends ValidatingQueryParamDecoderMatcher[Currency]("to")

}
