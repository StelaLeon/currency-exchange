package forex.http
package rates

import cats.effect.Concurrent
import cats.implicits.catsSyntaxTuple2Semigroupal
import cats.syntax.flatMap._
import forex.programs.RatesProgram
import forex.programs.rates.{Protocol => RatesProgramProtocol}
import org.http4s.HttpRoutes
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router

class RatesHttpRoutes[F[_]: Concurrent](rates: RatesProgram[F])
    extends Http4sDsl[F] {

  import Converters._
  import Protocol._
  import QueryParams._

  private[http] val prefixPath = "/rates"

  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {
    case GET -> Root :? FromQueryParam(fromRaw) +& ToQueryParam(toRaw) =>
      (fromRaw, toRaw)
        .mapN { (from, to) =>
          Concurrent
            .memoize(rates
              .get(RatesProgramProtocol.GetRatesRequest(from, to)))
            .flatten
            // this can be smarter in terms of cancelling the memoization in order for the new updated exchanges to happen
            .flatMap { rate =>
              rate.fold(
                _ =>
                  InternalServerError(
                    "We failed to retrieve currency exchange for you."),
                rate => Ok(rate.asGetApiResponse)
              )
            }
        }
        .valueOr(
          _ =>
            BadRequest(
              List(("FROM", fromRaw.isInvalid), ("TO", toRaw.isInvalid)) //eew!
                .map {
                  case (field, isV) =>
                    if (isV) s"unable to parse the '${field}' currency" else ""
                }
                .mkString("\n")
                .strip()
                .trim
          )
        )
  }
  val routes: HttpRoutes[F] = Router(
    prefixPath -> httpRoutes
  )

}
