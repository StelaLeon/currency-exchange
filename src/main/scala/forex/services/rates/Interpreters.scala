package forex.services.rates

import cats.Applicative
import cats.effect.{Async, Resource}
import forex.config.ExternalConfig
import forex.services.rates.interpreters._
import org.http4s.client.Client

object Interpreters {
  def dummy[F[_]: Applicative]: Algebra[F] = new OneFrameDummy[F]()
  def oneFrameLive[F[_]: Async](httpClient: Resource[F, Client[F]],
                                oneFrameConfig: ExternalConfig): Algebra[F] =
    new OneFrameLive[F](httpClient, oneFrameConfig)

}
