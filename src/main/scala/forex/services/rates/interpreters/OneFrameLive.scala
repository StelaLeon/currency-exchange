package forex.services.rates.interpreters

import cats.data.NonEmptyList
import cats.effect.{Async, Resource}
import cats.implicits.{
  catsSyntaxApplicativeId,
  catsSyntaxEitherId,
  toFlatMapOps
}
import forex.config.ExternalConfig
import forex.domain.Rate
import forex.http.rates.OneFrameResponse
import forex.programs.rates.errors.Error.RateLookupFailed
import forex.services.rates.Algebra
import forex.services.rates.errors.Error
import org.http4s._
import org.http4s.circe.jsonOf
import org.http4s.client.Client
import org.http4s.implicits.http4sLiteralsSyntax

import scala.util._

class OneFrameLive[F[_]: Async](httpClient: Resource[F, Client[F]],
                                oneFrameConfig: ExternalConfig)
    extends Algebra[F] {

  override def get(pair: Rate.Pair): F[Either[Error, Rate]] = {
    httpClient.use { client =>
      val request = Request[F](
        method = Method.GET,
        uri = uri"http://localhost:8080/rates".withQueryParams(
          Map("pair" -> s"${pair.from.toString + pair.to.toString}")),
        headers = Headers.of(
          Header("token", oneFrameConfig.apitoken),
        )
      )
      import forex.http.rates.OneFrameResponse._
      implicit val listDecoder = jsonOf[F, NonEmptyList[OneFrameResponse]]

      client
        .expectOr[NonEmptyList[OneFrameResponse]](request)(e =>
          throw RateLookupFailed(
            s"Oneframe Live external call failed with status: ${e.status}"))
        .flatMap(e => {
          toRate(e.head)
            .asRight[Error]
            .pure[F] // this can be extended to a list of rates if needed
        })
    }
  }
}
