package forex.http

import cats.Show
import cats.effect.{ContextShift, IO}
import cats.implicits.{catsSyntaxApplicativeId, catsSyntaxEitherId}
import forex.domain.Rate.Pair
import forex.domain.{Currency, Price, Rate, Timestamp}
import forex.http.rates.RatesHttpRoutes
import forex.programs.rates.errors.Error
import forex.programs.rates.{Algebra, Protocol}
import org.http4s.implicits.http4sLiteralsSyntax
import org.http4s._
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global

class RatesHttpRoutesSpec extends AnyFlatSpec with Matchers {
  import RatesHttpRoutesSpec._

  private val ratesHttpClient=new  Algebra[IO] {
    def get(request: Protocol.GetRatesRequest): IO[Error Either Rate] =
      someRate.asRight[Error].pure[IO]
  }
  private val ratesSuccessfulRoutes = new RatesHttpRoutes[IO](ratesHttpClient)


  it should "Respond OK.Success to the happy case" in {
    val response: IO[Response[IO]] = ratesSuccessfulRoutes.routes.run(
      Request(method = Method.GET, uri = uri"/rates?from=USD&to=EUR")
    ).value.flatMap(_ match {
      case None => IO.raiseError(new RuntimeException("Hit the fan!"))
      case Some(v) => v.pure[IO]
    })
    check[String](response, Status.Ok, Some(show.show(someRate))) shouldBe (true)
  }

  it should "Respond 404 to not finding the TO currency" in {
    val response: IO[Response[IO]] = ratesSuccessfulRoutes.routes.run(
      Request(method = Method.GET, uri = uri"/rates?from=USD&to=LEU")
    ).leftSide.value.flatMap(_ match{
      case None => IO.raiseError(new RuntimeException("Hit the fan!"))
      case Some(v)=> v.pure[IO]
    })
    check[String](response, Status.BadRequest, Some("unable to parse the 'TO' currency")) shouldBe (true)
  }

  it should "Respond 404 to not finding the FROM currency" in {
    val response: IO[Response[IO]] = ratesSuccessfulRoutes.routes.run(
      Request(method = Method.GET, uri = uri"/rates?from=RON&to=USD")
    ).leftSide.value.flatMap(_ match {
      case None => IO.raiseError(new RuntimeException("Hit the fan!"))
      case Some(v) => v.pure[IO]
    })
    check[String](response, Status.BadRequest, Some("unable to parse the 'FROM' currency")) shouldBe (true)
  }

}

object RatesHttpRoutesSpec {
  // Return true if match succeeds; otherwise false
  def check[A](actual: IO[Response[IO]],
               expectedStatus: Status,
               expectedBody: Option[A])(
                implicit ev: EntityDecoder[IO, A]
              ): Boolean = {
    val actualResp = actual.unsafeRunSync()
    val statusCheck = actualResp.status == expectedStatus
    val bodyCheck = expectedBody.fold[Boolean](
      actualResp.body.compile.toVector.unsafeRunSync().isEmpty)( // Verify Response's body is empty.
      expected => {actualResp.as[A].unsafeRunSync() == expected}
    )
    statusCheck && bodyCheck
  }

  implicit protected def CS(implicit ec: ExecutionContext): ContextShift[IO] =
    IO.contextShift(ec)

  implicit val show: Show[Rate] = Show.show { rate =>
    s"""{"from":"${rate.pair.from}","to":"${rate.pair.to}","price":${rate.price.value},"timestamp":"${rate.timestamp.value}"}""".stripMargin
  }

  private val someRate = Rate(Pair(from = Currency.EUR, to = Currency.CHF), Price(BigDecimal.decimal(23L)), Timestamp.now)

}
